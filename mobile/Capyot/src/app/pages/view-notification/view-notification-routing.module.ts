import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewNotificationPage } from './view-notification.page';

const routes: Routes = [
  {
    path: '',
    component: ViewNotificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewNotificationPageRoutingModule {}

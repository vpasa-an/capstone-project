import { Component, OnInit } from "@angular/core";
import {
  LoadingController,
  NavController,
  ToastController
} from "@ionic/angular";
import { AuthService } from "../../services/auth.service";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  constructor(
    private loading: LoadingController,
    private nav: NavController,
    private toast: ToastController,
    private authService: AuthService,
    private storage: Storage
  ) {}

  public phone_number: string = "";
  public password: any;
  ngOnInit() {}

  async login() {
    let loading = await this.loading.create({
      message: "Loggin in please wait...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
    if (this.phone_number) {
      //validate phone_number, can do later (priority level 0.5)
      //after validation of phone_number, log in
      let result: any = await this.authService.Login(this.phone_number);
      if (!result.error && result.data) {
        await this.storage.set("token", result.data.token.token);
        this.nav.navigateRoot("home");
      }
    } else {
      this.toastMessage("Please enter your phone number and try again");
    }
  }

  async signUp() {
    let loading = await this.loading.create({
      message: "Please wait...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
    this.nav.navigateRoot("signup");
  }

  async signup() {
    let loading = await this.loading.create({
      message: "Loggin in please wait...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
    this.nav.navigateRoot("signup");
  }

  async toastMessage(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "middle",
      mode: "ios"
    });
    return toast.present();
  }
}

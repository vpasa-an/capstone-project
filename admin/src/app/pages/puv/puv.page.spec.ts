import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PuvPage } from './puv.page';

describe('PuvPage', () => {
  let component: PuvPage;
  let fixture: ComponentFixture<PuvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PuvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

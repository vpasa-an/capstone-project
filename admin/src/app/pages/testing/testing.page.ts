import { Component, OnInit } from "@angular/core";
import { GeolocationService } from "../../services/geolocation.service";
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-testing",
  templateUrl: "./testing.page.html",
  styleUrls: ["./testing.page.scss"]
})
export class TestingPage implements OnInit {
  constructor(private geolocationService: GeolocationService) {}

  public map: any;
  public user_location: any = {};
  public key = environment.GOOGLE_MAPS_KEY;

  ngOnInit() {
    this.initMap();
  }
  async initMap() {
    this.user_location = await this.geolocationService.GetUserLocation();
    console.log(this.user_location.location);
    this.map = new google.maps.Map(document.getElementById("map"), {
      center: this.user_location.location,
      zoom: 15
    });
  }
}

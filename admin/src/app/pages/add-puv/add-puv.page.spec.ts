import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { AddPuvPage } from './add-puv.page';

describe('AddPuvPage', () => {
  let component: AddPuvPage;
  let fixture: ComponentFixture<AddPuvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPuvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddPuvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

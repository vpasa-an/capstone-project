import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddPuvPageRoutingModule } from './add-puv-routing.module';
import { AddPuvPage } from './add-puv.page';

import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {  NbSelectModule, NbCardModule, NbButtonModule, NbIconModule, NbCheckboxModule, NbInputModule, NbDatepickerModule, NbTooltipModule, NbUserModule, NbListModule } from '@nebular/theme';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddPuvPageRoutingModule,
    NbCardModule,
    NbSelectModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbTooltipModule,
    NbUserModule,
    NbListModule,
    GoogleMapsModule
  ],
  declarations: [AddPuvPage]
})
export class AddPuvPageModule { }


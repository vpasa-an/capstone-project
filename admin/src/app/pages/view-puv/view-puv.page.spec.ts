import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewPuvPage } from './view-puv.page';

describe('ViewPuvPage', () => {
  let component: ViewPuvPage;
  let fixture: ComponentFixture<ViewPuvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPuvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewPuvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

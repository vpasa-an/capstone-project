import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateNotificationPage } from './update-notification.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateNotificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateNotificationPageRoutingModule {}

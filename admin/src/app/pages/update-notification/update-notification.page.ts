import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, ToastController } from '@ionic/angular';
import { MessageService } from "../../services/message.service";
import { map } from "rxjs/operators";
import { HistoryService } from '../../services/history.service';


@Component({
  selector: 'app-update-notification',
  templateUrl: './update-notification.page.html',
  styleUrls: ['./update-notification.page.scss'],
})
export class UpdateNotificationPage implements OnInit {

  constructor(
    private events: Events,
    private nav: NavController,
    private notificationService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private toast: ToastController,
    private messageService: MessageService,
    private historyService: HistoryService
  ) { }
  users: Array<any> = [];
  public message: any
  today: number = Date.now();

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    this.init();
    this.getUserList();
    setInterval(() => {
      this.today = Date.now();
    }, 1);
  }

  async init() {
    let id = this.activatedRoute.snapshot.params.id
    console.log(id)
    this.messageService.getMessageListByid(id)
    .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ value: c.payload.val() }))
        )
      )
      .subscribe(messages => {
        this.message = messages[2].value
      });
  }

    //get all users in the firebase
    getUserList() {
      this.messageService
        .getUserList()
        .snapshotChanges()
        .pipe(
          map(changes =>
            changes.map(c => ({ key: c.payload.key}))
          )
        )
        .subscribe(users => {
          this.users = users
        });
    }

  async Submit() {
    if (this.validateFields()) {
      let id = this.activatedRoute.snapshot.params.id
      let data = {
        created_at: this.today,
        message: this.message
      }
      let result = this.messageService.updateMessage(id,data)
      
      //loop then send updated message to all users
      for (let index = 0; index < this.users.length; index++) {
        let id = this.users[index].key;
          let notif = this.messageService.createUserMessage(
            this.message,
            id,
            this.today
          );
      }
      let _data = {
        action_type: 'Update-Notification',
        message: this.message,
        date: this.today
      }
      let history = await this.historyService.Save(_data)
      if(result){
        this.toastMessageSuccess('Notification Successfully Updated')
        this.nav.navigateRoot('notification')
      }
      else {
          this.toastMessageError('Could not save notification')
        }
    } else {
      this.toastMessageError('Please check all the fields')
      this.init()
    }
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'success',
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'danger',
    });
    return toast.present();
  }

  validateFields() {
    if (this.message) {
      return true
    } else {
      return false
    }
  }

}

import { Component, OnInit } from "@angular/core";
import { Events, NavController, ToastController } from "@ionic/angular";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"]
})
export class RegisterPage implements OnInit {
  constructor(
    private nav: NavController,
    private events: Events,
    private toast: ToastController,
    private auth: AuthService
  ) {}

  public username: any;
  public password: any;
  public confirmpassword: any;
  public passwordSchema: any;
  public url: any;

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
  }

  async register() {
    if (this.validateAllFields()) {
      if (this.password === this.confirmpassword) {
        if (this.validatePassword(this.password) && this.validatePassword(this.username)) {
          let result: any = await this.auth.Register(
            this.username,
            this.password,
            this.url
          );
          if (!result.error && result.data) {
            this.toastMessageSuccess("New Admin Successfully Registered");
            this.nav.navigateRoot("dashboard");
          } else {
            this.toastMessageError(result.message);
          }
        } else {
          this.toastMessageError("Password or Username should at least 7 and have a number on it");
          this.password = "";
          this.confirmpassword = "";
        }
      } else {
        this.toastMessageError("Password does not match");
        this.password = "";
        this.confirmpassword = "";
      }
    } else {
        this.toastMessageError('Please populate all fields')
        this.clearAllFields()
    }
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'success',
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'danger',
    });
    return toast.present();
  }

  clearAllFields(){
      this.password = ""
      this.confirmpassword = ""
      this.username = ""
  }

  validatePassword(password) {
    if (password.length > 7) {
      if (this.isnumber(password)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  validateAllFields() {
    if (this.username && this.confirmpassword && this.password) {
      return true;
    } else {
      return false;
    }
  }

  isnumber(password: string) {
    var matches = password.match(/\d+/g);
    if (matches != null) {
      return true;
    } else {
      return false;
    }
  } 
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event:any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }


}

import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class GeolocationService {
  constructor(private http: HttpClient) {}

  async GetUserLocation() {
    let data = {};
    let result = await this.http.post(
      `https://www.googleapis.com/geolocation/v1/geolocate?key=${environment.GOOGLE_MAPS_KEY}
    `,
      data
    ).toPromise();
    return result;
  }
}

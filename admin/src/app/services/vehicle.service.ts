import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import ServerConfig from '../configs/server.config';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http: HttpClient,
    private storage: Storage) { }

  async Save(data) {
    let token = await this.storage.get("token")
    let vehicle = await this.http.post(`${ServerConfig.API}/api/vehicle/save?access_token=${token}`, data).toPromise()
    return vehicle
  }
  async Listing() {
    let token = await this.storage.get("token")
    let vehicles = await this.http.get(`${ServerConfig.API}/api/vehicle/listing?access_token=${token}`).toPromise()
    return vehicles
  }

  async GetById(id) {
    let token = await this.storage.get('token')
    return await this.http.get(`${ServerConfig.API}/api/vehicle/get/${id}?access_token=${token}`).toPromise()
  }
}

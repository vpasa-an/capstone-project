import { Injectable } from '@angular/core';
import {  CanActivate } from '@angular/router';
import { NavController} from '@ionic/angular';
import { UsersService } from '../../services/users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private nav: NavController,
    private users: UsersService) {

  }
  async canActivate() {
    try {
      let result : any = await this.users.GetCurrent()
      if (!result.error && result.data) {
        // this.events.publish('has_menu', true);
        // this.events.publish('has_header', true);
        return true;
      } else {
        this.nav.navigateRoot(['/login']);
        return false;
      }
    } catch (e) {
      this.nav.navigateRoot(['/login']);
      return false;
    }
  }
}

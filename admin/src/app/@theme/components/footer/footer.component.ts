import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span>918Kiss2u &copy; 2019</span>
  `,
})
export class FooterComponent {
}

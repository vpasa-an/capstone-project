import Tokens from '../modules/tokens'
import Users from '../modules/users'
import md5 from 'md5'
import jwt from 'jsonwebtoken'

process.env.SECRET_KEY = 'secret'

export const UserRoutes = [
    {
        method: 'GET',
        path: '/api/users/current',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let _token = req.auth.credentials.token
            let token = await Tokens.GetByToken(_token)
            if (token) {
                let user = await Users.GetCurrent(token.owner._id)
                if (user) {
                    return {
                        error: 0,
                        data: token,
                        message: 'Success in getting the current user'
                    }
                } else {
                    return {
                        error: 1,
                        data: null,
                        message: 'User is not authorized'
                    }
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'User is not authorized'
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/users/edit/current',
        handler: async (req, res) => {
            let password = md5(req.payload.password)
            let data = {
                _id: req.payload.id,
                username: req.payload.username,
                password: password,
                url: req.payload.url
            }
            let user = await Users.Save(data)
            if (user) {
                return {
                    error: 0,
                    data: user,
                    message: 'Password is successfully changed'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Error while changing password'
                }
            }
        }
    }
]
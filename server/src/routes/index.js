import { AuthRoutes } from "./auth";
import { RoutesRoutes } from "./routes";
import { NotificationRoutes } from "./notifications";
import { UserRoutes } from "./users";
import { VehicleRoutes } from "./vehicles";
import { HistoriesRoutes } from "./histories";

export const routes = [
  ...AuthRoutes,
  ...RoutesRoutes,
  ...NotificationRoutes,
  ...UserRoutes,
  ...VehicleRoutes,
  ...HistoriesRoutes
];
